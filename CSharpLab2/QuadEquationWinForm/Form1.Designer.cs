﻿namespace QuadEquationWinForm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxa = new System.Windows.Forms.TextBox();
            this.textBoxc = new System.Windows.Forms.TextBox();
            this.textBoxb = new System.Windows.Forms.TextBox();
            this.labela = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSolve = new System.Windows.Forms.Button();
            this.labelx1 = new System.Windows.Forms.Label();
            this.textBoxx1 = new System.Windows.Forms.TextBox();
            this.labelx2 = new System.Windows.Forms.Label();
            this.textBoxx2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxa
            // 
            this.textBoxa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxa.Location = new System.Drawing.Point(236, 103);
            this.textBoxa.Name = "textBoxa";
            this.textBoxa.Size = new System.Drawing.Size(147, 34);
            this.textBoxa.TabIndex = 0;
            this.textBoxa.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxc
            // 
            this.textBoxc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxc.Location = new System.Drawing.Point(236, 183);
            this.textBoxc.Name = "textBoxc";
            this.textBoxc.Size = new System.Drawing.Size(147, 34);
            this.textBoxc.TabIndex = 1;
            this.textBoxc.TextChanged += new System.EventHandler(this.textBoxc_TextChanged);
            // 
            // textBoxb
            // 
            this.textBoxb.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxb.Location = new System.Drawing.Point(236, 143);
            this.textBoxb.Name = "textBoxb";
            this.textBoxb.Size = new System.Drawing.Size(147, 34);
            this.textBoxb.TabIndex = 2;
            this.textBoxb.TextChanged += new System.EventHandler(this.textBoxb_TextChanged);
            // 
            // labela
            // 
            this.labela.AutoSize = true;
            this.labela.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labela.Location = new System.Drawing.Point(185, 106);
            this.labela.Name = "labela";
            this.labela.Size = new System.Drawing.Size(46, 28);
            this.labela.TabIndex = 3;
            this.labela.Text = "a = ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(183, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "b = ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(185, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 28);
            this.label2.TabIndex = 5;
            this.label2.Text = "c = ";
            // 
            // buttonSolve
            // 
            this.buttonSolve.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSolve.Location = new System.Drawing.Point(236, 233);
            this.buttonSolve.Name = "buttonSolve";
            this.buttonSolve.Size = new System.Drawing.Size(147, 39);
            this.buttonSolve.TabIndex = 6;
            this.buttonSolve.Text = " Посчитать";
            this.buttonSolve.UseVisualStyleBackColor = true;
            this.buttonSolve.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelx1
            // 
            this.labelx1.AutoSize = true;
            this.labelx1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelx1.Location = new System.Drawing.Point(175, 290);
            this.labelx1.Name = "labelx1";
            this.labelx1.Size = new System.Drawing.Size(56, 28);
            this.labelx1.TabIndex = 8;
            this.labelx1.Text = "x1 = ";
            this.labelx1.Visible = false;
            // 
            // textBoxx1
            // 
            this.textBoxx1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBoxx1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxx1.Location = new System.Drawing.Point(236, 287);
            this.textBoxx1.Name = "textBoxx1";
            this.textBoxx1.ReadOnly = true;
            this.textBoxx1.Size = new System.Drawing.Size(248, 34);
            this.textBoxx1.TabIndex = 7;
            this.textBoxx1.Visible = false;
            // 
            // labelx2
            // 
            this.labelx2.AutoSize = true;
            this.labelx2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelx2.Location = new System.Drawing.Point(175, 330);
            this.labelx2.Name = "labelx2";
            this.labelx2.Size = new System.Drawing.Size(56, 28);
            this.labelx2.TabIndex = 10;
            this.labelx2.Text = "x2 = ";
            this.labelx2.Visible = false;
            // 
            // textBoxx2
            // 
            this.textBoxx2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBoxx2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxx2.Location = new System.Drawing.Point(236, 327);
            this.textBoxx2.Name = "textBoxx2";
            this.textBoxx2.ReadOnly = true;
            this.textBoxx2.Size = new System.Drawing.Size(248, 34);
            this.textBoxx2.TabIndex = 9;
            this.textBoxx2.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelx2);
            this.Controls.Add(this.textBoxx2);
            this.Controls.Add(this.labelx1);
            this.Controls.Add(this.textBoxx1);
            this.Controls.Add(this.buttonSolve);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labela);
            this.Controls.Add(this.textBoxb);
            this.Controls.Add(this.textBoxc);
            this.Controls.Add(this.textBoxa);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxa;
        private System.Windows.Forms.TextBox textBoxc;
        private System.Windows.Forms.TextBox textBoxb;
        private System.Windows.Forms.Label labela;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSolve;
        private System.Windows.Forms.Label labelx1;
        private System.Windows.Forms.TextBox textBoxx1;
        private System.Windows.Forms.Label labelx2;
        private System.Windows.Forms.TextBox textBoxx2;
    }
}

