﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            double m, n;
            Console.Write("m = ");
            m = Convert.ToDouble(Console.ReadLine());
            Console.Write("n = ");
            n = Convert.ToDouble(Console.ReadLine());
            double z1 = ((m - 1) * Math.Sqrt(m) - (n - 1) * Math.Sqrt(n)) / (Math.Sqrt(Math.Pow(m, 3) * n) + m * n + Math.Pow(m, 2) - m);
            double z2 = (Math.Sqrt(m) - Math.Sqrt(n)) / m;
            Console.WriteLine("z1 = {0},z2 = {0}", z1, z2);
            Console.Read();
        }
    }
}
