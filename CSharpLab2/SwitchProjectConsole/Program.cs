﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchProjectConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            
                int menu;
                
            do
            {
                flag = true;
                
                Console.WriteLine("Выберите пункт меню:\n" +
                "1.Расшифровка дня недели.\n" +
                "2.Расшифровка месяца.\n" +
                "3. Выполнение операций над числами.\n" +
                "4. Выход"
                );
            Console.Write("--> ");
            menu = Convert.ToInt32(Console.ReadLine());
                Console.Clear();


                switch (menu)
                {
                    case 1:{
                            bool fl = true;
                            int dayweek;
                            do
                            {
                                fl = true;
                                Console.Write("Введите номер от 1 до 7 - ");
                                dayweek = Convert.ToInt32(Console.ReadLine());
                                switch (dayweek)
                                {
                                    case 1: { Console.WriteLine("Понедельник"); break; }
                                    case 2: { Console.WriteLine("Вторник"); break; }
                                    case 3: { Console.WriteLine("Среда"); break; }
                                    case 4: { Console.WriteLine("Четверг"); break; }
                                    case 5: { Console.WriteLine("Пятница"); break; }
                                    case 6: { Console.WriteLine("Суббота"); break; }
                                    case 7: { Console.WriteLine("Воскресенье"); break; }
                                    default: { Console.WriteLine("Данные введены неправильно!"); fl = false; break; }
                                }
                            } while (!fl);
                            flag = false;
                            break;
                        }
                    case 2:{
                        
                            bool fl = true;
                            int month;
                            do
                            {
                                fl = true;
                                Console.Write("Введите номер от 1 до 12 - ");
                                month = Convert.ToInt32(Console.ReadLine());
                                switch (month)
                                {
                                    
                                    case 1: { Console.WriteLine("Январь"); break; }
                                    case 2: { Console.WriteLine("Февраль"); break; }
                                    case 3: { Console.WriteLine("Март"); break; }
                                    case 4: { Console.WriteLine("Апрель"); break; }
                                    case 5: { Console.WriteLine("Май"); break; }
                                    case 6: { Console.WriteLine("Июнь"); break; }
                                    case 7: { Console.WriteLine("Июль"); break; }
                                    case 8: { Console.WriteLine("Август"); break; }
                                    case 9: { Console.WriteLine("Сентябрь"); break; }
                                    case 10: { Console.WriteLine("Октябрь"); break; }
                                    case 11: { Console.WriteLine("Ноябрь"); break; }
                                    case 12: { Console.WriteLine("Декабрь"); break; }
                                    default: { Console.WriteLine("Неверные данные!");fl = false; break; }
                                }
                            } while (!fl);
                            flag = false;
                            break;
                        }
                    case 3:
                        {
                            bool fl = true;
                            
                            int oper;
                            double a, b;
                            do
                            {
                                fl = true;
                                Console.WriteLine("1. Сложение\n" +
                                    "2. Умножение\n" +
                                    "3. Вычитание\n" +
                                    "4. Деление\n"
                                    );

                                Console.WriteLine("--> ");


                                oper = Convert.ToInt32(Console.ReadLine());
                                Console.Write("Введите a: ");
                                a = Convert.ToDouble(Console.ReadLine());
                                Console.Write("Введите b: ");
                                b = Convert.ToDouble(Console.ReadLine());
                                switch (oper)
                                {

                                    case 1: { Console.WriteLine($"a+b = {a + b}"); break; }
                                    case 2: { Console.WriteLine($"a-b = {a - b}"); break; }
                                    case 3: { Console.WriteLine($"a*b = {a * b}"); break; }
                                    case 4: { Console.WriteLine($"a/b = {a / b}"); break; }

                                    default:
                                        {
                                            Console.WriteLine("Неверные данные!");
                                            fl = false;
                                            break;
                                        }
                                }
                            }
                            while (!fl);
                            flag = false;
                            break;
                        }
                    case 4:
                        {
                             Environment.Exit(0);
                            break;
                        }
                   
                    default:
                        {



                            Console.WriteLine("Повторите ввод!\n");
                            flag = false;
                            break;
                            
                        }

                }
            } while (!flag);
        }
    }
}
