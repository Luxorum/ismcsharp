﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole7
{
    class Program
    {
        static void Main(string[] args)
        {
            double P;
            double S = 10;
            int K = 1 ;
            bool flag;
            
            
            do
            {
                Console.Write("Input P: ");
                flag = double.TryParse(Console.ReadLine(), out P);

                if (P <= 0 || P>=50) { flag = false; }
                if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
            } while (!flag);
            do
            {
                K++;
                S += S * (P/100);
                
                
            } while (S <= 200);
            Console.WriteLine($"Days = {K},Distance = {S:F3}");
        }
    }
}
