﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            double sum = 0;
            bool flag;
            do
            {
                Console.Write("Input N: ");
                flag = int.TryParse(Console.ReadLine(), out N);
                if(N <= 0) { flag = false; }
                if (!flag ) Console.WriteLine("Incorrect input!Repeat please: ");
                    } while (!flag);
            for(int i = 1; i <= N; i++)
            {
                sum += 1.0 / i;
            }
            Console.WriteLine($"Sum = {sum:F3}");
        }
    }
}
