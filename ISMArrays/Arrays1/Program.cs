﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            int n;
            Random rnd = new Random();
            Console.Write("Введите количество элементов массива: ");
            n = Convert.ToInt32(Console.ReadLine());
            double[] mas = new double[n];
            
            for(int i = 0; i < n; i++)
            {
                mas[i] = rnd.NextDouble()*(10)-5;
                Console.Write($"{mas[i]:F3} ");
                
            }
           
            Console.WriteLine("\n");
            
            Console.WriteLine($"Сумма отрицательных элементов массива = {FindSumOfMinusElements(mas):F3}");
            Console.WriteLine($"Сумма индексов положительных элементов массива = {FindSumOfPlusElementsIndexes(mas)}");
            Console.WriteLine($"Количество целых чисел элементов массива = {FindCountOfDividedElements(mas)}");
            Console.WriteLine($"Максимальный элемент = {FindMax(mas):F3},его индекс = {FindMaxIndex(mas)},по модулю = {FindMaxAbs(mas):F3}");
            



        }
        public static double FindSumOfMinusElements(double[] arr)
        {
            double sum = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < 0) {
                    sum += arr[i];
                        }
            }
            return sum;
        }
        public static int FindSumOfPlusElementsIndexes(double[] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                {
                    sum += i;
                }
            }
            return sum;
        }
        public static int FindCountOfDividedElements(double[] arr)
        {
            int count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] % 1 == 0)
                {
                    count++;
                }
            }
            return count;
        }
        public static double FindMax(double[] arr)
        {
            double max = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            return max;
        }
        public static int FindMaxIndex(double[] arr)
        {
            double max = arr[0];
            int maxi = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    maxi = i;
                }
            }
            return maxi;
        }
        public static double FindMaxAbs(double[] arr)
        {
            double maxabs = arr[0];
            
            for (int i = 0; i < arr.Length; i++)
            {
                if (Math.Abs(arr[i]) > Math.Abs(maxabs))
                {
                    maxabs = arr[i];
                }
            }
            return maxabs;
        }

    }
}
