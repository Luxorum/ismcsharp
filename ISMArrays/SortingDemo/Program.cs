﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortingAlgorithms;

namespace SortingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            
            double[] mas;
            mas = FillArray();
            double[] selmas = mas;
            double[] insmas = mas;
            double[] shellmas = mas;
            double[] bubblemas = mas;
            double[] mergemas = mas;
            double[] quickmas = mas;
            Console.WriteLine("Your array: ");

            FillHyphens();
            
            Sorting.Display(mas);
            FillHyphens();
           
            Console.WriteLine("Selection Sorting: ");
            FillHyphens();
            Sorting.Display(Sorting.Selection(selmas));
            FillHyphens();
            Console.WriteLine("Inserting Sorting: ");
            FillHyphens();
            Sorting.Display(Sorting.Inserting(insmas));
            FillHyphens();
            Console.WriteLine("Bubble Sorting: ");
            FillHyphens();
            Sorting.Display(Sorting.Bubble(bubblemas));
            FillHyphens();
            Console.WriteLine("Shell Sorting: ");
            FillHyphens();
            Sorting.Display(Sorting.Shell(shellmas));
            FillHyphens();
            Console.WriteLine("Merge Sorting: ");
            FillHyphens();
            Sorting.Display(Sorting.Merge_Sort(mergemas));
            FillHyphens();
            Console.WriteLine("Quick Sorting: ");
            FillHyphens();
            Sorting.QuickSort_Sort(quickmas, 0, quickmas.Length - 1);
            Sorting.Display(quickmas);
            FillHyphens();

        }
        static double[] FillArray()
        {
            bool flag;
            int n;
            double a, b;
            
            do
            {
                Console.Write("Enter number of array elements: ");
                flag = int.TryParse(Console.ReadLine(), out n);
                if (n <= 0) flag = false;
                if (!flag) Console.WriteLine("Incorrect input!Try again!");
            } while (!flag);
            do
            {
                Console.Write("Enter lower number of random: ");
                flag = double.TryParse(Console.ReadLine(), out a);

                if (!flag) Console.WriteLine("Incorrect input!Try again!");
            } while (!flag);
            do
            {
                Console.Write("Enter upper number of random: ");
                flag = double.TryParse(Console.ReadLine(), out b);

                if (!flag) Console.WriteLine("Incorrect input!Try again!");
            } while (!flag);
            double[] mas =  new double[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.NextDouble() * (b - a) + a;
            }
            return mas;

        }
        static void FillHyphens()
        {
            for (int i = 0; i < 81; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine("");
        }
    }
    

}
