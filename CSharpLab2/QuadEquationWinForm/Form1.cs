﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuadEquationWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            labelx1.Visible = false;
            textBoxx1.Visible = false;
            labelx2.Visible = false;
            textBoxx2.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool flag = true;
            double a, b, c;
            do
            {
                flag = double.TryParse(textBoxa.Text, out a);
                if (Convert.ToInt32(textBoxa.Text) == 0) flag = false;
                if (!flag) MessageBox.Show("Помилка вводу данних!","Помилка",MessageBoxButtons.OK,MessageBoxIcon.Error);
            } while (!flag);
            do
            {
                flag = double.TryParse(textBoxa.Text, out b);
                if (!flag) MessageBox.Show("Помилка вводу данних!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } while (!flag);
            do
            {
                flag = double.TryParse(textBoxa.Text, out c);
                if (!flag) MessageBox.Show("Помилка вводу данних!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } while (!flag);
            double d = Math.Pow(-b, 2) - 4 * a * c;
            if (d < 0)
            {
                MessageBox.Show("Рішень немає!", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                double x1 = (-b - Math.Sqrt(d)) / 2 * a;
                double x2 = (-b + Math.Sqrt(d)) / 2 * a;
                if(x1 == x2)
                {
                    labelx1.Visible = true;
                    labelx1.Text = "x = ";
                    textBoxx1.Text = x1.ToString();
                }
                else
                {
                    labelx1.Visible = true;
                    labelx1.Text = "x1 = ";
                    labelx2.Visible = true;
                    textBoxx1.Visible = true;
                    textBoxx2.Visible = true;
                    textBoxx1.Text = x1.ToString();
                    textBoxx2.Text = x2.ToString();
                }
            }

        }

        private void textBoxb_TextChanged(object sender, EventArgs e)
        {
            labelx1.Visible = false;
            textBoxx1.Visible = false;
            labelx2.Visible = false;
            textBoxx2.Visible = false;
        }

        private void textBoxc_TextChanged(object sender, EventArgs e)
        {
            labelx1.Visible = false;
            textBoxx1.Visible = false;
            labelx2.Visible = false;
            textBoxx2.Visible = false;
        }
    }
}
