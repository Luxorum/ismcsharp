﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            double a, b;
            Random rnd = new Random();
            Console.Write("Введите количество элементов массива: ");

            n = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите нижнюю границу рандома: ");
            a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите верхнюю границу рандома: ");
            b = Convert.ToInt32(Console.ReadLine());
            double[] mas = new double[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.NextDouble() * (b - a) + a;
                Console.Write($"{mas[i]:F3} ");

            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine($"Произведение элементов после минимального элемента = {MultiplyAfterMinimum(mas)}");



            Console.WriteLine($"Сумма элементов между первым отрицательным и вторым положительным элементом = {SumBetweenFirstMinusAndSecondPlus(mas):F3}");
            Console.WriteLine($"Сумма элементов между первым нулем и последним нулем = {SumBetweenNulls(mas):F3}");
            Console.WriteLine($"Произведение элементов между максимальным и минимальным за модулем = {MultiplyBetweenAbsolutes(mas):F3}");
            
        }
        public static double MultiplyAfterMinimum(double[] arr)
        {
            double multiply = 1;
            var min = arr[0];
            int mini = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    mini = i;
                }
            }
            for (int i = mini + 1; i < arr.Length; i++)
            {
                multiply *= arr[i];
            }

            return multiply;
        }
        public static double SumBetweenFirstMinusAndSecondPlus(double[] arr)
        {
            double sum = 0, pluscount = 0;
            int firstindex = 0, secondindex = 0;



            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                {
                    pluscount++;

                }

                if (pluscount == 2)
                {
                    secondindex = i;
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < 0)
                {
                    firstindex = i;
                    break;

                }


            }
            if (firstindex < secondindex)
            {
                for (int i = firstindex + 1; i < secondindex; i++)
                {
                    sum += arr[i];
                }
            }
            else if (secondindex < firstindex)
            {
                for (int i = secondindex + 1; i < firstindex; i++)
                {
                    sum += arr[i];
                }
            }

            return sum;
        }
        public static double SumBetweenNulls(double[] arr)
        {
            int firstnull = 0, lastnull = 0, nullcount = 0;
            double sum = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0 && nullcount == 0)
                {
                    nullcount++;
                    firstnull = i;
                    continue;
                }
                else if (arr[i] == 0)
                {
                    lastnull = i;
                }
            }
            for (int i = firstnull + 1; i < lastnull; i++)
            {
                sum += arr[i];
            }

            return sum;
        }
        
        public static double MultiplyBetweenAbsolutes(double[] arr)
        {
            int minabsi = 0, maxabsi = 0;
            double maxabs = arr[0], minabs = arr[0];
            double mul = 1;

            for (int i = 0; i < arr.Length; i++)
            {
                if (Math.Abs(arr[i]) > Math.Abs(maxabs))
                {
                    maxabs = arr[i];
                    maxabsi = i;
                }
                if (Math.Abs(arr[i]) < Math.Abs(minabs))
                {
                    minabs = arr[i];
                    minabsi = i;
                }
            }
            
            if (maxabsi > minabsi)
            {
                
                for (int i = minabsi + 1; i < maxabsi; i++)
                {
                    mul *= arr[i];
                    
                }
            }
            else if(maxabsi < minabsi)
            {
                for (int i = maxabsi + 1; i < minabsi; i++)
                {
                    mul *= arr[i];
                    
                }
            }
            
            return mul;
        }
    }

















}
