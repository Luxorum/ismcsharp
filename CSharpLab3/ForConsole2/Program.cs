﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, B;
            int sqrsum = 0;
            bool flag;
            do
            {
               
                Console.Write("Input A: ");
                flag = int.TryParse(Console.ReadLine(), out A);
                Console.Write("Input B: ");
                flag = int.TryParse(Console.ReadLine(), out B);
                if (A > B) { flag = false; }
                if (!flag)
                {
                    
                    Console.WriteLine("Incorrect input!Repeat please: ");
                }
                } while (!flag);
           

            for (int i = A; i <= B; i++)
            {
                
                sqrsum += Convert.ToInt32(Math.Pow(i, 2));
                
            }
            Console.WriteLine($"Sum = {sqrsum}");
        }
    }
}
