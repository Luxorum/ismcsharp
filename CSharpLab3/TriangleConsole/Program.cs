﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            double x, y, x1, y1, x2, y2, x3, y3;
            Console.WriteLine("Enter x: ");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter x1: ");
            x1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter y1: ");
            y1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter x2: ");
            x2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter y2: ");
            y2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter x3: ");
            x3 = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter y3: ");
            y3 = double.Parse(Console.ReadLine());
            double AB = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            double BC = Math.Sqrt(Math.Pow(x3 - x2, 2) + Math.Pow(y3 - y2, 2));
            double AC = Math.Sqrt(Math.Pow(x3 - x1, 2) + Math.Pow(y3 - y1, 2));
            
            double pp = (AB + BC + AC) / 2.0;

            double S1 = Math.Sqrt(pp * (pp - AB) * (pp - BC) * (pp - AC));
            double AD = Math.Sqrt(Math.Pow(x-x1,2) + Math.Pow(y-y1,2));
            double BD = Math.Sqrt(Math.Pow(x - x2, 2) + Math.Pow(y - y2, 2));
            double CD = Math.Sqrt(Math.Pow(x - x3, 2) + Math.Pow(y - y3, 2));
            double pp1 = (AB + BD + AD) / 2.0;
            double pp2 = (BD + CD + BC) / 2.0;
            double pp3 = (AD + CD + AC) / 2.0;
            double S11 = Math.Sqrt(pp1 * (pp1 - AB) * (pp1- BD) * (pp1 - AD));
            double S12 = Math.Sqrt(pp2 * (pp2 - BD) * (pp2 - CD) * (pp2 - BC));
            double S13 = Math.Sqrt(pp3 * (pp3 - AD) * (pp3 - CD) * (pp3 - AC));
            double S2 = S11 + S12 + S13;
            Console.WriteLine($"S2 = {S2}");
            Console.WriteLine($"S1 = {S2}");
            if (S1 == S2)
            {
                Console.WriteLine("Точка находится внутри треугольника");
            }
            else
            {
                Console.WriteLine("Точка находится снаружи");
            }

        }
    }
}
