﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, a, b;
            Console.Write("x = ");
            x = Convert.ToDouble(Console.ReadLine());
            Console.Write("a = ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.Write("b = ");
            b = Convert.ToDouble(Console.ReadLine());
            double y = 2.40 * Math.Abs((Math.Pow(x, 2) + b) / a) + (a - b) * Math.Pow(Math.Sin(a - b), 2) + (Math.Pow(10, -2)) * (x - b);
            Console.WriteLine("y = {0}", y);
            Console.Read();
        }
    }
}
