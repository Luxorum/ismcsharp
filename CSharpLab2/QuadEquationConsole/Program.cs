﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadEquationConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            bool flag = true;
            double a, b, c;
            do
            {
                Console.Write("a = ");
                flag = Double.TryParse(Console.ReadLine(), out a);
                if (a == 0) flag = false;
                if (!flag) Console.WriteLine("Повторите ввод!");
            }
            while (!flag);
            do
            {
                Console.Write("b = ");
                flag = Double.TryParse(Console.ReadLine(), out b);
                
                if (!flag) Console.WriteLine("Повторите ввод!");
            }
            while (!flag);
            do
            {
                Console.Write("c = ");
                flag = Double.TryParse(Console.ReadLine(), out c);
                if (!flag) Console.WriteLine("Повторите ввод!");
            }
            while (!flag);
            double d = Math.Pow(-b, 2) - 4 * a * c;
            if (d < 0)
            {
                Console.WriteLine("Нету решений");
            }
            else
            {
                double x1 = (-b - Math.Sqrt(d)) / 2 * a;
                double x2 = (-b + Math.Sqrt(d)) / 2 * a;
                if (x1 == x2)
                {
                    Console.WriteLine($"x = {x1:F3}");
                }
                else
                {
                    Console.WriteLine($"x1 = {x1:F3}");
                    Console.WriteLine($"x2 = {x2:F3}");

                }
            }

            
        }
    }
    }

