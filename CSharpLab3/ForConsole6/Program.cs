﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole6
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            int K = 0;
            bool flag;
            bool check;
            double res = 0;
            do
            {
                Console.Write("Input N: ");
                flag = int.TryParse(Console.ReadLine(), out N);
                
                if (N <= 0 ) { flag = false; }
                if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
            } while (!flag);
            do
            {
                
                check = Math.Pow(3, K) > N;
                if (check) break;
                    K++;
            } while (!check);
            Console.WriteLine($"Min value of K is {K}");
        }
    }
}
