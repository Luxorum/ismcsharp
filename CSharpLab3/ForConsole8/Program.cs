﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole8
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            bool flag;
            int sum = 0;
            int i = 0;
            do
            {
                i++;
                do
                {
                    Console.Write($"Input integer number {i}: ");
                    flag = int.TryParse(Console.ReadLine(), out N);


                    if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
                } while (!flag);
                sum += N;
            } while (N != 0);
            Console.WriteLine($"Sum = {sum}");
        }
    }
}
