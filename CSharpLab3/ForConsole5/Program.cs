﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole5
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            int K;
            bool flag;
            double sum = 0;
            do
            {
                Console.Write("Input N: ");
                flag = int.TryParse(Console.ReadLine(), out N);
                Console.Write("Input K: ");
                flag = int.TryParse(Console.ReadLine(), out K);
                if (N <= 0 || K<=0) { flag = false; }
                if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
            } while (!flag);
            for (int i = 1; i <= N; i++)
            {
                sum += Math.Pow(i, K);
            }
            Console.WriteLine($"Sum = {sum:F0}");
        }
    }
}
