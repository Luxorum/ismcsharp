﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole4
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            bool flag;
            double sum = 0;
            do
            {
                Console.Write("Input N: ");
                flag = int.TryParse(Console.ReadLine(), out N);
                if (N <= 0) { flag = false; }
                if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
            } while (!flag);
            for(double i = 1; i <= N; i++)
            {
                
                {
                    sum += Math.Pow(i, N - (i - 1));
                }
                
            }
            Console.WriteLine($"Sum = {sum}");
        }
    }
}
