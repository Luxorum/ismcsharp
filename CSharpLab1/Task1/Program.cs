﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            int s = a * b;
            int p = (a + b) * 2;
            Console.WriteLine("Периметр - {0},Площа - {1}", p, s);
            Console.Read();
        }
    }
}
