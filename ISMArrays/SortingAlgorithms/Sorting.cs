﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public class Sorting
    {
        public static void Display(double [] mas)
        {
            Console.Write("[");
            for (int i = 0; i < mas.Length; i++)
            {

                if (i == mas.Length - 1)
                {
                    Console.WriteLine($"{mas[i],7:F3}]");
                    break;
                }
                if (i != 0 && i % 10 == 0)
                {
                    Console.Write("\n|");
                }
                Console.Write($"{mas[i],7:F3}|");



            }
        }
        public static double[] Shell(double[] mas)
        {
            double tmp = 0;
            int i, j, k, n = mas.Length;

            for (k = n / 2; k > 0; k /= 2)
            {
                for (i = k; i < n; i++)
                {
                    tmp = mas[i];
                    for (j = i; j >= k; j -= k)
                    {
                        if (tmp < mas[j - k])
                        {
                            mas[j] = mas[j - k];
                        }
                        else break;
                    }
                    mas[j] = tmp;
                }
            }


            return mas;
        }

        public static double[] Selection(double[] mas)
        {
            double tmp = 0;
            for(int i = 0; i < mas.Length-1; i++)
            {
                for(int j = i+1; j < mas.Length; j++)
                {
                    if (mas[j] < mas[i])
                    {
                        tmp = mas[j];
                        mas[j] = mas[i];
                        mas[i] = tmp;
                    }
                }
            }
            return mas;
        }
        public static double[] Inserting(double[] mas)
        {
            double tmp;
            for(int i = 1; i < mas.Length; i++)
            {
                for(int j = 0; j <= i - 1; j++)
                {
                    if (mas[i] < mas[j])
                    {
                        tmp = mas[j];
                        mas[j] = mas[i];
                        mas[i] = tmp;
                        
                    }
                }
            }
            return mas;
        }
        public static double[] Bubble(double[] mas)
        {
            double tmp = 0;
            for(int i = 0; i < mas.Length - 1; i++)
            {
                for (int j = 0; j < mas.Length-1; j++)
                {
                    if (mas[j] > mas[j + 1])
                    {
                        tmp = mas[j];
                        mas[j] = mas[j + 1];
                        mas[j + 1] = tmp;
                    }
                }
            }
            return mas;
        }

        public static double[] Merge_Merge(double[] maspart1,double[] maspart2)
        {
            int a = 0, b = 0;
            double[] merged = new double[maspart1.Length + maspart2.Length];
            for (int i = 0; i < merged.Length; i++)
            {
                if (a < maspart1.Length && b < maspart2.Length)
                {
                    if (maspart1[a] > maspart2[b])
                        merged[i] = maspart2[b++];
                    else merged[i] = maspart1[a++];
                }
                else
                {
                    if (b < maspart2.Length)
                        merged[i] = maspart2[b++];
                    else merged[i] = maspart1[a++];
                }
                    
            }
            return merged;
        }
        public static double[] Merge_Sort(double[] mas)
        {
            if (mas.Length == 1)
            {
                return mas;
            }
            int middle = mas.Length / 2;
            return Merge_Merge(Merge_Sort(mas.Take(middle).ToArray()), Merge_Sort(mas.Skip(middle).ToArray()));//Здесь используются функции Take и Skip позволяющие вернуть первую часть элементов до середины и вторую часть после середины.
        }

        public static int QuickSort_ToParts(double[]mas,int first,int last)
        {
            double tmp;
            int current = first;
            for (int i = first; i <= last; i++)
            {
                if (mas[i] < mas[last])
                {
                    tmp = mas[current];
                    mas[current] = mas[i];
                    mas[i] = tmp;
                    current++;
                }

            }
            tmp = mas[current];
            mas[current] = mas[last];
            mas[last] = tmp;
            return current;
        }
        public static void QuickSort_Sort(double []mas,int first,int last)
        {
            if (first >= last)
            {
                return;
            }
            int pivot = QuickSort_ToParts(mas, first, last);
            QuickSort_Sort(mas, first, pivot - 1);
            QuickSort_Sort(mas, pivot+1, last);

        }
    }
}
