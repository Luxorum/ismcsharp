﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForConsole3
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            bool flag;
            double fact = 1;
            do
            {
                Console.Write("Input N: ");
                flag = int.TryParse(Console.ReadLine(), out N);
                if (N <= 0) { flag = false; }
                if (!flag) Console.WriteLine("Incorrect input!Repeat please: ");
            } while (!flag);
            for(double i = 2; i <= N; i++)
            {
                fact *= i;
            }
            Console.WriteLine($"Factorial = {fact:F3}");
        }
    }
}
