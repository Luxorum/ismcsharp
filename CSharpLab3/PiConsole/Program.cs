﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            
            
            
            bool flag;
            int iterations;
            int menu;
            Console.WriteLine("Choose type of solution:" +
                "\n1.Viet" +
                "\n2.Wallis" +
                "\n3.Lord Brunker" +
                "\n4.Leibnits");

            do
            {
                Console.Write("-->");
                flag = int.TryParse(Console.ReadLine(), out menu);
                if (!flag) Console.WriteLine("Incorrect input!Try again!");

            } while (!flag);
            do
            {
                Console.Write("Input number of iterations: ");
                flag = int.TryParse(Console.ReadLine(), out iterations);
                if (!flag) Console.WriteLine("Incorrect input!Try again!");

            } while (!flag);
            switch (menu)
            {
                case 1:
                    {
                        double pi = Math.Sqrt(0.5);
                        double tmp = pi;
                        for (double i = 1; i <= iterations; i++)
                        {

                               tmp  = Math.Sqrt((0.5) + (0.5) * tmp);
                                
                                pi*= tmp;
                      
                                
                        }
                        pi = 2 / pi;
                        Console.WriteLine($"pi = {pi}");

                        break;
                    }
                case 2:
                    {
                        double secpi = 1;
                        for(double i = 1; i <=iterations; i++)
                        {
                            if (i % 2 != 0)
                            {
                                
                                secpi *= (i * (2 + i)) / ((i+1)*(i+1));
                            }
                        }
                        secpi = 2.0 / secpi;
                        Console.WriteLine($"Secpi = {secpi}");
                        break;
                    }
                case 3:
                    {
                        double pi = 1;
                        for (int i = 1; i < iterations; i++) {
                            if (i == 1)
                            {
                                pi /= 1 + Math.Pow(i, 2)/2;
                            }
                            if (i != 1 && i % 2 !=0)
                            {
                                pi +=  Math.Pow(i, 2) / 2; 
                            }
                        }
                        break;

                        
                    }
                case 4:
                    {
                        double pi = 0;
                        double tmp = 0;
                    
                        
                        for(double i = 1; i <= iterations; i+=4)
                        {
                            
                                tmp = (1.0 / i) - (1.0 / (2 + i));
                            pi += tmp;
                            
                        }
                        pi = pi * 4.0;
                        Console.WriteLine($"pi = {pi}");
                        break;
                    }
                default: {
                        Environment.Exit(0);
                        break;
                    }
                    
                    
            }
        }
    }
}
