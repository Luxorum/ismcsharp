﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Drawing.SystemColors;
namespace SwitchProjectWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            if (button1.BackColor == Color.Green)
            {

                try
                {

                    switch (Convert.ToInt32(textBoxInput.Text))
                    {

                        case 1: { labelRes.Visible = true; labelRes.Text = "Понедельник"; break; }
                        case 2: { labelRes.Visible = true; labelRes.Text = "Вторник"; break; }
                        case 3: { labelRes.Visible = true; labelRes.Text = "Среда"; break; }
                        case 4: { labelRes.Visible = true; labelRes.Text = "Четверг"; break; }
                        case 5: { labelRes.Visible = true; labelRes.Text = "Пятница"; break; }
                        case 6: { labelRes.Visible = true; labelRes.Text = "Суббота"; break; }
                        case 7: { labelRes.Visible = true; labelRes.Text = "Воскресенье"; break; }
                        default: { labelRes.Visible = false; MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); break; }
                    }
                }
                catch
                {
                    labelRes.Visible = false;
                    MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (button2.BackColor == Color.Green)
            {

                try
                {
                    
                    switch (Convert.ToInt32(textBoxInput.Text))
                    {

                        case 1: { labelRes.Visible = true; labelRes.Text = "Январь"; break; }
                        case 2: { labelRes.Visible = true; labelRes.Text = "Февраль"; break; }
                        case 3: { labelRes.Visible = true; labelRes.Text = "Март"; break; }
                        case 4: { labelRes.Visible = true; labelRes.Text = "Апрель"; break; }
                        case 5: { labelRes.Visible = true; labelRes.Text = "Май"; break; }
                        case 6: { labelRes.Visible = true; labelRes.Text = "Июнь"; break; }
                        case 7: { labelRes.Visible = true; labelRes.Text = "Июль"; break; }
                        case 8: { labelRes.Visible = true; labelRes.Text = "Август"; break; }
                        case 9: { labelRes.Visible = true; labelRes.Text = "Сентябрь"; break; }
                        case 10: { labelRes.Visible = true; labelRes.Text = "Октябрь"; break; }
                        case 11: { labelRes.Visible = true; labelRes.Text = "Ноябрь"; break; }
                        case 12: { labelRes.Visible = true; labelRes.Text = "Декабрь"; break; }
                        default: { labelRes.Visible = false; MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); break; }
                    }
                }
                catch
                {
                    labelRes.Visible = false;
                    MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (button3.BackColor == Color.Green)
            {

                try
                {
                    bool flag = true;
                    
                    double a, b;
                    do
                    {
                        flag = double.TryParse(textBoxa.Text, out a);
                        if (!flag) MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } while (!flag);
                    do
                    {
                        flag = double.TryParse(textBoxb.Text, out b);
                        if (!flag) MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } while (!flag);
                    switch (textBoxOperation.Text)
                    {

                        case "+": { labelRes.Visible = true;label3.Visible = true; labelRes.Text = (a+b).ToString(); break; }
                        case "-": { labelRes.Visible = true; label3.Visible = true; labelRes.Text = (a - b).ToString(); break; }
                        case "*": { labelRes.Visible = true; label3.Visible = true; labelRes.Text = (a*b).ToString(); break; }
                        case "/": { labelRes.Visible = true; label3.Visible = true; labelRes.Text = (a / b).ToString(); break; }
                        
                        default: { labelRes.Visible = false; MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); break; }
                    }
                }
                catch
                {
                    labelRes.Visible = false;
                    MessageBox.Show("Неверные данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button1.BackColor = Color.Green;
            
            button3.Enabled = false;
            button2.Enabled = false;
            button4.Enabled = false;
            buttonRepick.Visible = true;
            textBoxInput.Visible = true;
            buttonOK.Visible = true;
            
        }

        private void buttonRepick_Click(object sender, EventArgs e)
        {
            textBoxInput.Clear();
            button1.Enabled = true;
            button3.Enabled = true;
            button2.Enabled = true;
            button4.Enabled = true;
            buttonRepick.Visible = false;
            textBoxInput.Visible = false;
            buttonOK.Visible = false;
            labelRes.Visible = false;
            button1.BackColor = Color.FromArgb(128, 255, 255);
            button2.BackColor = Color.FromArgb(255, 224, 192);
            button3.BackColor = Color.FromArgb(255, 255, 192);
            button4.BackColor = Color.FromArgb(255, 192, 255);
            buttonOK.Location = new Point(130, 114);
            labelRes.Location = new Point(15, 158);
            textBoxa.Visible = false;
            textBoxb.Visible = false;
            textBoxOperation.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
        }

        private void textBoxInput_TextChanged(object sender, EventArgs e)
        {
            textBoxInput.Text.Trim();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.BackColor = Color.Green;

            button3.Enabled = false;
            button2.Enabled = false;
            button4.Enabled = false;
            buttonRepick.Visible = true;
            textBoxInput.Visible = true;
            buttonOK.Visible = true;
            
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button3.BackColor = Color.Green;

            button3.Enabled = false;
            button2.Enabled = false;
            button4.Enabled = false;
            buttonRepick.Visible = true;
            
            buttonOK.Visible = true;
            buttonOK.Location = new Point(418, 178);
            labelRes.Location = new Point(240, 213);
            textBoxa.Visible = true;
            textBoxb.Visible = true;
            textBoxOperation.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
